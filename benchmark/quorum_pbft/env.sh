QUO_HOME=/users/tsalman/blockbench/benchmark/quorum_pbft
HOSTS=$QUO_HOME/hosts
CLIENTS=$QUO_HOME/clients
QUO_DATA=/users/tsalman/quorum_pbft
LOG_DIR=$QUO_HOME/pbftresults_1
EXE_HOME=$QUO_HOME/../../src/macro/kvstore
BENCHMARK=ycsb
QUORUM=/usr/local/bin/geth
ADDRESSES=$QUO_HOME/addresses

##comment these out for smallbank
EXE_HOME=$QUO_HOME/../../src/macro/smallbank
BENCHMARK=smallbank 
#LOG_DIR=$QUO_HOME/smallbank_results_2
